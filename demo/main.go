package main

import (
	"fmt"
	"log"
	"net/http"

	"ai-portal/consul"

	"github.com/gorilla/mux"
)

const (
	consulAddress = "18.218.216.250:8500"
	// ServicePort is port where this service will run
	ServicePort = 8080
	// ServiceName of this service
	ServiceName = "DEMO_SERVICE"
)

func main() {
	consulClient := consul.GetConsulClient()
	consulClient.Register(ServiceName, ServicePort)
	defer consulClient.DeRegister(ServiceName)
	go consulClient.UpdateTTL(consulClient.Check)
	router := mux.NewRouter()
	router.HandleFunc("/demo", GetDemo).Methods("GET")
	log.Fatal(http.ListenAndServe(":8080", router))
}

// GetDemo handle demo request
func GetDemo(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "{\"message\":\"successfully handled\", \"success\":true}")
}
