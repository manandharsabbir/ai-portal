//ai-portal/constants/parameter-constants.go

package constants

const (
	// ParamSrcLang is name of source language parameter
	ParamSrcLang = "from"
	// ParamTrgLang points to target langauge parameter
	ParamTrgLang = "to"
	// ParamData points to data to translate
	ParamData = "data"
	// ParamTextType points to type of the input data
	ParamTextType = "textType"
	// ParamContentType indicates the content type of input data
	ParamContentType = "content-type"
	// ParamProfileID points to Profile ID to use
	ParamProfileID = "profileId"
	// ParamTermManager points to term manager URL
	ParamTermManager = "tmgr"
	// ParamDomain points to domain of the engine
	ParamDomain = "domain"
	// ParamAPIKey points to api key for the engine
	ParamAPIKey = "apikey"
	// ParamWorkflowID points current workflow Id
	ParamWorkflowID = "workflowId"
	// ParamMtProfileID parameter name for MtProfileID
	ParamMtProfileID = "mtprofileId"
	// ParamOrganizationID parameter name for organization ID
	ParamOrganizationID = "organizationId"
	// ParamSourceLanguages parameter name for source languages
	ParamSourceLanguages = "sourceLanguages"
	// ParamTargetLanguages parameter name for target languages
	ParamTargetLanguages = "targetLanguages"
	// ParamSourceDefault parameter name for default source langauge
	ParamSourceDefault = "sourceDefault"
	// ParamTargetDefault parameter name for default target language
	ParamTargetDefault = "targetDefault"
	// ParamGlossaryID parameter anme for glossary Id
	ParamGlossaryID = "glossaryId"
	// ParamTM parameter anme for TM
	ParamTM = "tm"
	// ParamEngines parameter name engines
	ParamEngines = "engines"
	// ParamAPIKeys parameter name for API keys
	ParamAPIKeys = "apiKeys"
	// ParamAuthrization paramter name for authorization
	ParamAuthrization = "authorization"
	// ParamEngineID parameter name for engine Id
	ParamEngineID = "engineId"
	// ParamJobID parameter name for job Id
	ParamJobID = "jobId"
	// ParamFileID parameter name for file Id
	ParamFileID = "fileId"
	// ParamUserID  parameter name for user Id
	ParamUserID = "userId"
	// ParamStartTimestamp parameter name for start timestamp
	ParamStartTimestamp = "startTimestamp"
	// ParamEndTimestamp parameter name for end timestamp
	ParamEndTimestamp = "endTimestamp"
	// ParamWordCount parameter name for word count
	ParamWordCount = "wordCount"
	// ParamCharCount parameter name for char count
	ParamCharCount = "charCount"
	// ParamRequestType parameter name for request type
	ParamRequestType = "requestType"
	// ParamMimeType parameter name for mime type
	ParamMimeType = "mimeType"
	// ParamSourceFileURL parameter name for source file name or URL
	ParamSourceFileURL = "fileNameUrl"
	// ParamErrorMessage parameter name for error message
	ParamErrorMessage = "errorMessage"
	// ParamTags parameter name for tags
	ParamTags = "tags"
	// ParamIP parameter name for IP
	ParamIP = "ip"
	// ParamGlossary parameter name for glossary
	ParamGlossary = "glossary"
	// ParamOCRCount  parameter name for ocrCount
	ParamOCRCount = "ocrCount"
	// ParamUserRole parameter name for user role
	ParamUserRole = "userRole"
)
