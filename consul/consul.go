package consul

import (
	"fmt"
	"log"
	"net"
	"os"
	"time"

	consul "github.com/hashicorp/consul/api"
)

//---------------------------------------------------------------------------------------

const (
	// DefaultConsulHTTPAddr points to default location where consul is running
	DefaultConsulHTTPAddr = "localhost:8500"
	// ServiceCheckTTL time for which consul will consider the service is alive. Service has
	// to update TTL to keep itself alive
	ServiceCheckTTL = 30 * time.Second
) // const

//---------------------------------------------------------------------------------------

var consulClient Client
var localIPAddress string

//---------------------------------------------------------------------------------------

func init() {
	localIPAddress = GetLocalIP()
	if err := createConsulClient(); err != nil {
		log.Fatalf("init: Error creating the consul client to connect to consul agent: %s", err)
	}
} // init

//---------------------------------------------------------------------------------------

// Client provides an interface for getting data out of Consul
type Client interface {
	// SErvices Get all services from consul
	Services(string, string) ([]*consul.ServiceEntry, *consul.QueryMeta, error)
	// Service Get single services from consul
	Service(string, string) (string, error)
	// Register registers a service to consul agent
	Register(string, int) error
	// DeRegister deregisters a service from consul agent
	DeRegister(string) error
	// UpdateTTL sends message to consule agent signifying its activeness
	UpdateTTL(func() (bool, error))
	// Check returns response of service health check
	Check() (bool, error)
} // Client

//---------------------------------------------------------------------------------------

type client struct {
	TTL    time.Duration
	consul *consul.Client
} // client

//---------------------------------------------------------------------------------------

// GetConsulClient returns a Client interface for given consul address to interact
// (register, deregister, get service) with consul agent
func GetConsulClient() Client {
	return consulClient
} // GetConsulClient

//---------------------------------------------------------------------------------------

// createConsulClient creates an instance of ConsulClient
func createConsulClient() error {
	config := consul.DefaultConfig()
	config.Address = GetConsulAddress()
	c, err := consul.NewClient(config)
	if err != nil {
		return err
	}
	consulClient = &client{
		TTL:    ServiceCheckTTL,
		consul: c,
	}
	return nil
} // createConsulClient

//---------------------------------------------------------------------------------------

// Register a service with consul local agent
func (c *client) Register(name string, port int) error {
	log.Printf("Register: Registering %s to consul agent", name)
	reg := &consul.AgentServiceRegistration{
		ID:      name,
		Name:    name,
		Address: localIPAddress,
		Port:    port,
		Check: &consul.AgentServiceCheck{
			TTL: c.TTL.String(),
		},
	}
	err := c.consul.Agent().ServiceRegister(reg)
	if err == nil {
		log.Printf("Register: Registered %s to consul agent", name)
		return nil
	}
	log.Printf("Register: ERROR Could not register %s to consul agent: %s", name, err)
	return err
} // Register

//---------------------------------------------------------------------------------------

// DeRegister a service with consul local agent
func (c *client) DeRegister(id string) error {
	log.Printf("DeRegistering %s to consul agent", id)
	return c.consul.Agent().ServiceDeregister(id)
} // DeRegister

//---------------------------------------------------------------------------------------

// Service return a service
func (c *client) Service(service, tag string) (string, error) {
	addrs, _, err := c.Services(service, tag)
	if err != nil {
		return "", err
	}
	serviceEntry := addrs[0]
	serviceURL := fmt.Sprintf("%s:%d", serviceEntry.Service.Address, serviceEntry.Service.Port)
	return serviceURL, nil
} // Service

//---------------------------------------------------------------------------------------

// Services returns all services
func (c *client) Services(service, tag string) ([]*consul.ServiceEntry, *consul.QueryMeta, error) {
	passingOnly := true
	addrs, meta, err := c.consul.Health().Service(service, tag, passingOnly, nil)
	if len(addrs) == 0 && err == nil {
		return nil, nil, fmt.Errorf("service ( %s ) was not found", service)
	}
	if err != nil {
		return nil, nil, err
	}
	return addrs, meta, nil
} // Services

//---------------------------------------------------------------------------------------

func (c *client) UpdateTTL(check func() (bool, error)) {
	ticker := time.NewTicker(c.TTL / 2)
	for range ticker.C {
		c.update(check)
	}
} // UpdateTTL

//---------------------------------------------------------------------------------------

func (c *client) update(check func() (bool, error)) {
	ok, err := check()
	if !ok {
		log.Printf("err=\"Check failed\" msg=\"%s\"", err.Error())
		if agentErr := c.consul.Agent().FailTTL("service:", err.Error()); agentErr != nil {
			log.Print(agentErr)
		}
	} else {
		if agentErr := c.consul.Agent().PassTTL("service:DEMO_SERVICE", ""); agentErr != nil {
			log.Print(agentErr)
		}
	}
} // update

//---------------------------------------------------------------------------------------

func (c *client) Check() (bool, error) {
	return true, nil
} // Check

//---------------------------------------------------------------------------------------

// GetConsulAddress returns the address of the consul agent
func GetConsulAddress() string {
	consulHTTPAddr := os.Getenv(consul.HTTPAddrEnvName)
	if consulHTTPAddr == "" {
		return DefaultConsulHTTPAddr
	}
	return consulHTTPAddr
} // GetConstulAddress

//---------------------------------------------------------------------------------------

// GetLocalIP returns the non loopback local IP of the host
func GetLocalIP() string {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		log.Printf("GetLocalIP: Error Identifying Local IP address. Returning empty. Error: %s", err)
		return ""
	}
	for _, address := range addrs {
		// check the address type and if it is not a loopback then return it
		if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				return ipnet.IP.String()
			}
		}
	}
	log.Print("GetLocalIP: Could not find Local IP address. Returning empty.")
	return ""
} // GetLocalIP
