//ai-portal/usage-service/handler.go
package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"

	"ai-portal/constants"

	"github.com/gorilla/mux"
)

// SaveUsageStats handles request to save usage statistics
func SaveUsageStats(w http.ResponseWriter, r *http.Request) {
	log.Print("request for save usage stats")
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Fprintf(w, "{\"result\": \"%s\", \"success\" : false}", err.Error())
		return
	}
	log.Println(string(body))
	err = usageService.SaveUsageStats(body)
	if err != nil {
		fmt.Fprintf(w, "{\"result\": \"%s\", \"success\" : false}", err.Error())
		return
	}
	fmt.Fprintf(w, "{\"result\": \"Stat successfully saved.\", \"success\" : true}")
}

//---------------------------------------------------------------------------------------

// ReadUsageStats searches for the stats with given search parameters
func ReadUsageStats(w http.ResponseWriter, r *http.Request) {
	log.Print("request for search usage stats")
	orgIDStr := r.Header.Get(constants.ParamOrganizationID)
	if orgIDStr == "" {
		fmt.Fprintf(w, "{\"message\" : \"organizationId is missing from header.\", \"success\" : false}")
		return
	}
	orgID, err := strconv.ParseInt(orgIDStr, 10, 32)
	if err != nil {
		fmt.Fprintf(w, "{\"message\" : \"Failed to read usage stats. Invalid organizationId %s received\", \"success\" : false}", orgIDStr)
		return
	}

	userIDStr := r.Header.Get(constants.ParamUserID)
	if userIDStr == "" {
		fmt.Fprintf(w, "{\"message\" : \"userId is missing from header.\", \"success\" : false}")
		return
	}
	userID, err := strconv.ParseInt(userIDStr, 10, 32)
	if err != nil {
		fmt.Fprintf(w, "{\"message\" : \"Failed to read usage stats. Invalid userId %s received\", \"success\" : false}", userIDStr)
		return
	}

	userRole := r.Header.Get(constants.ParamUserRole)
	if userRole == "" {
		fmt.Fprintf(w, "{\"message\" : \"usrRole is missing from header.\", \"success\" : false}")
		return
	}

	result, err := usageService.ReadUsageStats(int32(orgID), int32(userID), userRole)
	if err != nil {
		fmt.Fprintf(w, "{\"result\": \"%s\", \"success\" : false}", err.Error())
		return
	}
	fmt.Fprintf(w, "{\"result\": %s, \"success\" : true}", result)
}

//---------------------------------------------------------------------------------------

// UpdateWorkflowUsageStats saves the usage statistics of current workflow
func UpdateWorkflowUsageStats(w http.ResponseWriter, r *http.Request) {
	log.Print("request for save workflow usage stats")
	params := mux.Vars(r)
	workflowIDStr := params[constants.ParamWorkflowID]
	workflowID, err := strconv.ParseInt(workflowIDStr, 10, 32)
	if err != nil {
		fmt.Fprintf(w, "{\"message\" : \"Failed to Update current workflow usage stats. Invalid workflowID %s received\", \"success\" : false}", workflowIDStr)
		return
	}
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Fprintf(w, "{\"result\": \"%s\", \"success\" : false}", err.Error())
		return
	}
	log.Println(string(body))
	err = usageService.UpdateWorkflowUsageStats(body, int32(workflowID))
	if err != nil {
		fmt.Fprintf(w, "{\"result\": \"%s\", \"success\" : false}", err.Error())
		return
	}
	fmt.Fprintf(w, "{\"result\": \"Stat successfully updated.\", \"success\" : true}")
}

//---------------------------------------------------------------------------------------

// DeleteWorkflowUsageStats deletes the usage statistics of current workflow
func DeleteWorkflowUsageStats(w http.ResponseWriter, r *http.Request) {
	log.Print("request for delete usage stats")
	params := mux.Vars(r)
	workflowIDStr := params[constants.ParamWorkflowID]
	workflowID, err := strconv.ParseInt(workflowIDStr, 10, 32)
	if err != nil {
		fmt.Fprintf(w, "{\"message\" : \"Failed to Delete current workflow usage stats. Invalid workflowID %s received\", \"success\" : false}", workflowIDStr)
		return
	}
	err = usageService.DeleteWorkflowUsageStats(int32(workflowID))
	if err != nil {
		fmt.Fprintf(w, "{\"result\": \"%s\", \"success\" : false}", err.Error())
		return
	}
	fmt.Fprintf(w, "{\"result\": \"Stat successfully Deleted.\", \"success\" : true}")
}

//---------------------------------------------------------------------------------------
