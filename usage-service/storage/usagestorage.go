// ai-portal/usage-service/storage/usagestorage.go

package storage

import "time"

// UsageStorage defines operations related to Usage Stats Storage
type UsageStorage interface {
	SaveUsageStats(usageStat *UsageStat) error
	ReadUsageStats(organizationID, userID int32, userRole string) (string, error)
	UpdateWorkflowUsageStats(usageStat *UsageStat) error
	DeleteWorkflowUsageStats(workflowID int32) error
}

// UsageStat representing details about usage stat
type UsageStat struct {
	WorkflowID     int32
	JobID          int32
	FileID         int32
	OrganizationID int32
	UserID         int32
	from           string
	to             string
	StartTimestamp time.Time
	EndTimestamp   time.Time
	WordCount      int32
	CharCount      int32
	RequestType    string
	MimeType       string
	FileNameURL    string
	ErrorMessage   string
	Tags           string
	IP             string
	Tm             string
	Tmgr           string
	Glossary       string
	OCRCount       int32
}
