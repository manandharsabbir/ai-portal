// ai-portal/usage-service/storage/mockedusagestorage.go

package storage

import (
	"encoding/json"
	"fmt"
)

//---------------------------------------------------------------------------------------

// MockedUsageStorage mocked storage service
type MockedUsageStorage struct {
	statStorage map[int32]*UsageStat
}

// NewMockedStorage creates new instance for MockedUsageStorage
func NewMockedStorage() *MockedUsageStorage {
	return &MockedUsageStorage{
		statStorage: make(map[int32]*UsageStat, 0),
	}
}

//---------------------------------------------------------------------------------------

// SaveUsageStats saves usageStat into storage
func (storage *MockedUsageStorage) SaveUsageStats(usageStat *UsageStat) error {
	storage.statStorage[usageStat.WorkflowID] = usageStat
	return nil
}

//---------------------------------------------------------------------------------------

// ReadUsageStats returns all stats in the storage
func (storage *MockedUsageStorage) ReadUsageStats(organizationID, userID int32, userRole string) (string, error) {
	if len(storage.statStorage) == 0 {
		return "", fmt.Errorf("No Usage Stats are available")
	}
	orgUsageStats := make([]*UsageStat, 0)
	for _, stat := range storage.statStorage {
		if organizationID == stat.OrganizationID {
			orgUsageStats = append(orgUsageStats, stat)
		}
	}
	if len(orgUsageStats) == 0 {
		return "", fmt.Errorf("No Usage Stats are available")
	}
	data, err := json.Marshal(orgUsageStats)
	if err != nil {
		return "", err
	}
	return string(data), nil
}

//---------------------------------------------------------------------------------------

// UpdateWorkflowUsageStats updates current workflow usage stat
func (storage *MockedUsageStorage) UpdateWorkflowUsageStats(usageStat *UsageStat) error {
	if usageStat == nil {
		return fmt.Errorf("UsageStat to update cannot be nil")
	}
	_, ok := storage.statStorage[usageStat.WorkflowID]
	if ok {
		storage.statStorage[usageStat.WorkflowID] = usageStat
		return nil
	}
	return fmt.Errorf("UsageStat with workflowId %v does not exist", usageStat.WorkflowID)
}

//---------------------------------------------------------------------------------------

// DeleteWorkflowUsageStats deletes current workflow usage stat
func (storage *MockedUsageStorage) DeleteWorkflowUsageStats(workflowID int32) error {
	_, ok := storage.statStorage[workflowID]
	if ok {
		delete(storage.statStorage, workflowID)
		return nil
	}
	return fmt.Errorf("No Usage Stat exists for workflowID: %v", workflowID)
}
