// ai-portal/usage-service/service/usageservice.go

package service

import (
	"ai-portal/usage-service/storage"
	"encoding/json"
	"fmt"
)

//---------------------------------------------------------------------------------------

// UsageService is an interface spcifying operations for Usage stats
type UsageService interface {
	SaveUsageStats(jsonBody []byte) error
	ReadUsageStats(organizationID, userID int32, userRole string) (string, error)
	UpdateWorkflowUsageStats(jsonBody []byte, workflowID int32) error
	DeleteWorkflowUsageStats(workflowID int32) error
}

//---------------------------------------------------------------------------------------

// PortalUsageService service for usage stat related operations
type PortalUsageService struct {
	usageStorage storage.UsageStorage
}

//---------------------------------------------------------------------------------------

// NewPortalUsageService craetes new instance of PortalUsageService
func NewPortalUsageService(usageStorage storage.UsageStorage) *PortalUsageService {
	return &PortalUsageService{
		usageStorage: usageStorage,
	}
}

//---------------------------------------------------------------------------------------

// SaveUsageStats saves usageStat into storage
func (service *PortalUsageService) SaveUsageStats(jsonBody []byte) error {
	var usageStat storage.UsageStat
	err := json.Unmarshal(jsonBody, &usageStat)
	if err != nil {
		return fmt.Errorf("Error in decoding the input JSON: %s", err.Error())
	}
	return service.usageStorage.SaveUsageStats(&usageStat)
}

//---------------------------------------------------------------------------------------

// ReadUsageStats returns all stats in the storage
func (service *PortalUsageService) ReadUsageStats(organizationID, userID int32, userRole string) (string, error) {
	return service.usageStorage.ReadUsageStats(organizationID, userID, userRole)
}

// UpdateWorkflowUsageStats updates current workflow usage stat
func (service *PortalUsageService) UpdateWorkflowUsageStats(jsonBody []byte, workflowID int32) error {
	var usageStat storage.UsageStat
	err := json.Unmarshal(jsonBody, &usageStat)
	if err != nil {
		return fmt.Errorf("Error in decoding the input JSON: %s", jsonBody)
	}
	usageStat.WorkflowID = workflowID
	return service.usageStorage.UpdateWorkflowUsageStats(&usageStat)
}

// DeleteWorkflowUsageStats deletes current workflow usage stat
func (service *PortalUsageService) DeleteWorkflowUsageStats(workflowID int32) error {
	return service.usageStorage.DeleteWorkflowUsageStats(workflowID)
}
