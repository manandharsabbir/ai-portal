// ai-portal/usage-service/main.go
package main

import (
	"log"
	"net/http"

	"ai-portal/usage-service/service"
	"ai-portal/usage-service/storage"

	"github.com/gorilla/mux"
)

//---------------------------------------------------------------------------------------

const (
	port = ":8000"
)

var usageStorage storage.UsageStorage
var usageService service.UsageService

//---------------------------------------------------------------------------------------

// main function to run the service
func main() {
	usageStorage = storage.NewMockedStorage()
	usageService = service.NewPortalUsageService(usageStorage)

	router := mux.NewRouter()
	router.HandleFunc("/usageStatistics", SaveUsageStats).Methods("POST")
	router.HandleFunc("/usageStatistics/query", ReadUsageStats).Methods("POST")
	router.HandleFunc("/usageStatistics/{workflowId}", UpdateWorkflowUsageStats).Methods("PUT")
	router.HandleFunc("/usageStatistics/{workflowId}", DeleteWorkflowUsageStats).Methods("DELETE")
	log.Fatal(http.ListenAndServe(port, router))
}

//---------------------------------------------------------------------------------------
