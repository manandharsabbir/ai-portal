package main

import (
	myconsul "ai-portal/consul"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
)

func main() {
	consulClient := myconsul.GetConsulClient()
	serviceURL, err := consulClient.Service("DEMO_SERVICE", "")
	if err != nil {
		log.Fatalf("Error reading service information from consul agent: %s", err)
	}

	fmt.Println(serviceURL)
	res, err := http.Get("http://" + serviceURL + "/demo")
	if err != nil {
		fmt.Println(err)
		return
	}

	io.Copy(os.Stdout, res.Body)
	res.Body.Close()
}
