//ai-portal/mtengine/handler.go

package main

import (
	"ai-portal/constants"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

//---------------------------------------------------------------------------------------

// GetTranslation translate the input text
func GetTranslation(w http.ResponseWriter, r *http.Request) {
	log.Print("request for Translation")
	jwtToken := r.Header.Get(constants.ParamAuthrization)
	if jwtToken == "" {
		fmt.Fprintf(w, "{\"message\" : \"JWT Token is missing.\", \"success\" : false}")
		return
	}
	srcData := r.FormValue(constants.ParamData)
	srcLang := r.FormValue(constants.ParamSrcLang)
	trgLang := r.FormValue(constants.ParamTrgLang)
	textType := r.FormValue(constants.ParamTextType)
	contentType := r.FormValue(constants.ParamContentType)
	mtProfileIDStr := r.FormValue(constants.ParamProfileID)
	mtProfileID, err := strconv.ParseInt(mtProfileIDStr, 10, 32)
	if err != nil {
		fmt.Fprintf(w, "{\"message\" : \"MtProfile read Failed. Invalid MtProfileID %s received\", \"success\" : false}", mtProfileIDStr)
		return
	}
	termManager := r.FormValue(constants.ParamTermManager)
	domain := r.FormValue(constants.ParamDomain)
	apiKey := r.FormValue(constants.ParamAPIKey)
	workflowIDStr := r.FormValue(constants.ParamWorkflowID)
	workflowID, err := strconv.ParseInt(workflowIDStr, 10, 32)
	if err != nil {
		fmt.Fprintf(w, "{\"message\" : \"Translation Failed. Invalid workflowID %s received\", \"success\" : false}", workflowIDStr)
		return
	}
	log.Printf("Parameters: SrcLang: %s, TrgLang: %s, TextType: %s, ContentType: %s, ProfileId: %v, TermManager: %s, Domain: %s, ApiKey: %s, WorkflowID: %v",
		srcLang, trgLang, textType, contentType, mtProfileID, termManager, domain, apiKey, workflowID)

	result, err := mtEngineService.GetTranslation(jwtToken, srcData, srcLang, trgLang, textType, contentType, int32(mtProfileID), termManager, domain, apiKey, int32(workflowID))
	if err != nil {
		fmt.Fprintf(w, "{\"message\" : \"%s\", \"success\" : false}", err.Error())
		return
	}
	fmt.Fprintf(w, "{\"message\" : \"%s\", \"success\" : true}", result)
}

//---------------------------------------------------------------------------------------

// GetEngineInfo retrieve information about engine
func GetEngineInfo(w http.ResponseWriter, r *http.Request) {
	log.Print("request for Engine Information")
	params := mux.Vars(r)
	engineIDStr := params[constants.ParamEngineID]
	engineID, err := strconv.ParseInt(engineIDStr, 10, 32)
	if err != nil {
		fmt.Fprintf(w, "{\"message\" : \"Failed to read engine info. Invalid EngineID %s received\", \"success\" : false}", engineIDStr)
		return
	}
	result, err := mtEngineService.GetEngineInfo(int32(engineID))
	if err != nil {
		fmt.Fprintf(w, "{\"message\" : \"%s\", \"success\" : false}", err.Error())
		return
	}
	fmt.Fprint(w, result)
}

//---------------------------------------------------------------------------------------

// CreateEngine creates new engine
func CreateEngine(w http.ResponseWriter, r *http.Request) {
	log.Print("request for Engine Creation")
	result, err := mtEngineService.CreateEngine()
	if err != nil {
		fmt.Fprintf(w, "{\"message\" : \"%s\", \"success\" : false}", err.Error())
		return
	}
	fmt.Fprint(w, result)
}

//---------------------------------------------------------------------------------------

// DeleteEngine deletes engine
func DeleteEngine(w http.ResponseWriter, r *http.Request) {
	log.Print("request for Engine Deletion")
	result, err := mtEngineService.DeleteEngine()
	if err != nil {
		fmt.Fprintf(w, "{\"message\" : \"%s\", \"success\" : false}", err.Error())
		return
	}
	fmt.Fprint(w, result)
}

//---------------------------------------------------------------------------------------

//GetMtProfile retrieves mtProfile with given mtProfilID
func GetMtProfile(w http.ResponseWriter, r *http.Request) {
	log.Print("request for Profile Information")
	params := mux.Vars(r)
	mtProfileIDStr := params[constants.ParamMtProfileID]
	mtProfileID, err := strconv.ParseInt(mtProfileIDStr, 10, 32)
	if err != nil {
		fmt.Fprintf(w, "{\"message\" : \"MtProfile read Failed. Invalid MtProfileID %s received\", \"success\" : false}", mtProfileIDStr)
		return
	}
	msg, err := mtProfileService.GetMtProfile(int32(mtProfileID))
	if err != nil {
		fmt.Fprintf(w, "{\"message\" : \"%s\", \"success\" : false}", err.Error())
		return
	}
	fmt.Fprintf(w, "{\"message\" : %s, \"success\" : true}", msg)
}

//---------------------------------------------------------------------------------------

//GetMtProfiles retrieves all mtProfiles of organization
func GetMtProfiles(w http.ResponseWriter, r *http.Request) {
	log.Print("request for All organization MtProfiles")
	params := mux.Vars(r)
	mtOrgIDStr := params[constants.ParamOrganizationID]
	mtOrgID, err := strconv.ParseInt(mtOrgIDStr, 10, 32)
	if err != nil {
		fmt.Fprintf(w, "{\"message\" : \"Organization MtProfiles read Failed. Invalid MtProfileID %v received\", \"success\" : false}", mtOrgIDStr)
		return
	}
	result, err := mtProfileService.GetMtProfiles(int32(mtOrgID))
	if err != nil {
		fmt.Fprintf(w, "{\"message\" : \"%s\", \"success\" : false}", err.Error())
		return
	}
	fmt.Fprintf(w, "{\"message\" : %s, \"success\" : true}", result)
}

//---------------------------------------------------------------------------------------

//CreateMtProfile creates new MtProfile
func CreateMtProfile(w http.ResponseWriter, r *http.Request) {
	log.Print("request for New MtProfile")
	orgIDStr := r.FormValue(constants.ParamOrganizationID)
	orgID, err := strconv.ParseInt(orgIDStr, 10, 32)
	if err != nil {
		fmt.Fprintf(w, "{\"message\" : \"MtProfile create Failed. Invalid OrganizationId '%s' received\", \"success\" : false}", orgIDStr)
		return
	}
	sourceLanguages := r.FormValue(constants.ParamSourceLanguages)
	targetLanguages := r.FormValue(constants.ParamTargetLanguages)
	sourceDefault := r.FormValue(constants.ParamSourceDefault)
	targetDefault := r.FormValue(constants.ParamTargetDefault)
	glossaryIDStr := r.FormValue(constants.ParamGlossaryID)
	glossaryID, err := strconv.ParseInt(glossaryIDStr, 10, 32)
	if err != nil {
		fmt.Fprintf(w, "{\"message\" : \"MtProfile create Failed. Invalid GlossaryID %s received\", \"success\" : true}", glossaryIDStr)
		return
	}
	tm := r.FormValue(constants.ParamTM)
	engines := r.FormValue(constants.ParamEngines)
	apiKeys := r.FormValue(constants.ParamAPIKeys)
	message, err := mtProfileService.CreateMtProfile(int32(orgID), sourceLanguages, targetLanguages, sourceDefault, targetDefault, int32(glossaryID), tm, engines, apiKeys)
	if err != nil {
		fmt.Fprintf(w, "{\"message\" : \"%s\", \"success\" : false}", err.Error())
		return
	}
	fmt.Fprintf(w, "{\"message\" : \"%s\", \"success\" : true}", message)
}

//---------------------------------------------------------------------------------------

//DeleteMtProfile deletes an MtProfile
func DeleteMtProfile(w http.ResponseWriter, r *http.Request) {
	log.Print("request for Deletion of MtProfile")
	params := mux.Vars(r)
	mtProfileIDStr := params[constants.ParamMtProfileID]
	mtProfileID, err := strconv.ParseInt(mtProfileIDStr, 10, 32)
	if err != nil {
		fmt.Fprintf(w, "{\"message\" : \"Delete Failed. Invalid MtProfileID %s received\", \"success\" : true}", mtProfileIDStr)
		return
	}
	msg, err := mtProfileService.DeleteMtProfile(int32(mtProfileID))
	if err != nil {
		fmt.Fprintf(w, "{\"message\" : \"%s\", \"success\" : false}", err.Error())
		return
	}
	fmt.Fprintf(w, "{\"message\" : \"%s\", \"success\" : true}", msg)
}

//---------------------------------------------------------------------------------------

//UpdateMtProfile updates an MtProfile
func UpdateMtProfile(w http.ResponseWriter, r *http.Request) {
	log.Print("request for Update of MtProfile")
	params := mux.Vars(r)
	mtProfileIDStr := params[constants.ParamMtProfileID]
	mtProfileID, err := strconv.ParseInt(mtProfileIDStr, 10, 32)
	if err != nil {
		fmt.Fprintf(w, "{\"message\" : \"Delete Failed. Invalid MtProfileID %s received\", \"success\" : true}", mtProfileIDStr)
		return
	}
	sourceLanguages := r.FormValue(constants.ParamSourceLanguages)
	targetLanguages := r.FormValue(constants.ParamTargetLanguages)
	sourceDefault := r.FormValue(constants.ParamSourceDefault)
	targetDefault := r.FormValue(constants.ParamTargetDefault)
	glossaryIDStr := r.FormValue(constants.ParamGlossaryID)
	glossaryID, err := strconv.ParseInt(glossaryIDStr, 10, 32)
	if err != nil {
		fmt.Fprintf(w, "{\"message\" : \"MtProfile create Failed. Invalid GlossaryID %s received\", \"success\" : true}", glossaryIDStr)
		return
	}
	tm := r.FormValue(constants.ParamTM)
	engines := r.FormValue(constants.ParamEngines)
	apiKeys := r.FormValue(constants.ParamAPIKeys)
	message, err := mtProfileService.UpdateMtProfile(int32(mtProfileID), sourceLanguages, targetLanguages, sourceDefault, targetDefault, int32(glossaryID), tm, engines, apiKeys)
	if err != nil {
		fmt.Fprintf(w, "{\"message\" : \"%s\", \"success\" : false}", err.Error())
		return
	}
	fmt.Fprintf(w, "{\"message\" : \"%s\", \"success\" : true}", message)
}

//---------------------------------------------------------------------------------------
