// ai-portal/mtengine/utils/utils.go

package utils

import "time"

// MtProfile struct to represent MtProfile
type MtProfile struct {
	MtProfileID     int32     `json:"mtProfileID,omit"`
	OrganizationID  int32     `json:"organizationID"`
	SourceLanguages string    `json:"sourceLanguages"`
	TargetLanguages string    `json:"targetLanguages"`
	SourceDefault   string    `json:"sourceDefault,omitempty"`
	TargetDefault   string    `json:"targetDefault,omitempty"`
	GlossaryID      int32     `json:"glossaryID"`
	Tm              string    `json:"tm"`
	Engines         string    `json:"engines"`
	APIKeys         string    `json:"apiKeys"`
	CreatedDate     time.Time `json:",omit"`
	LastModified    time.Time `json:",omit"`
}

// MtProfilService is an interface for MtProfile related operations
type MtProfilService interface {
	GetMtProfile(mtProfileID int32) (message string, err error)
	GetMtProfiles(organizationID int32) (message string, err error)
	CreateMtProfile(organizationID int32, sourceLanguages, targetLanguages, sourceDefault, targetDefault string, glossaryID int32, tm, engines, apiKeys string) (message string, err error)
	DeleteMtProfile(mtProfileID int32) (message string, err error)
	UpdateMtProfile(mtProfileID int32, sourceLanguages, targetLanguages, sourceDefault, targetDefault string, glossaryID int32, tm, engines, apiKeys string) (message string, err error)
}

// MtEngineService is an interface for MtEngine related operations
type MtEngineService interface {
	GetTranslation(jwtToken, srcText, srcLang, trgLang, textType, contentType string, profileID int32, tmmgr, domain, apiKey string, workflowID int32) (result string, err error)
	GetEngineInfo(engineID int32) (result string, err error)
	CreateEngine() (result string, err error)
	DeleteEngine() (result string, err error)
}
