// ai-portal/mtengine/utils/mockedMtEngineService.go

package utils

import (
	"fmt"
)

// MockedMtEngineService mocked service for MtEngine operations
type MockedMtEngineService struct {
	MtProfileService MtProfilService
}

// GetTranslation get translation for given input
func (service *MockedMtEngineService) GetTranslation(jwtToken, srcText, srcLang, trgLang, textType, contentType string, profileID int32, tmmgr, domain, apiKey string, workflowID int32) (result string, err error) {
	if jwtToken == "" {
		return "", fmt.Errorf("JWT Token is missing")
	}
	result, err = service.MtProfileService.GetMtProfile(profileID)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("This is mocked translation for: %s", srcText), nil
}

// GetEngineInfo get information about engine
func (service *MockedMtEngineService) GetEngineInfo(engineID int32) (result string, err error) {
	return fmt.Sprintf("{\"ID\": \"%v\" \"vendor\" : \"google\", \"domains\" : [\"generalnn\", \"statistical\"]}", engineID), nil
}

// CreateEngine create new engine
func (service *MockedMtEngineService) CreateEngine() (result string, err error) {
	return "{\"message\" : \"Engine Created!\", \"success\" : true}", nil
}

// DeleteEngine delete engine
func (service *MockedMtEngineService) DeleteEngine() (result string, err error) {
	return "{\"message\" : \"Engine Deleted!\", \"success\" : true}", nil
}
