// ai-portal/mtengine/utils/mockedService.go

package utils

import (
	"encoding/json"
	"errors"
	"fmt"
	"time"
)

// MockedService mocked service for mtProfile operations
type MockedService struct {
	ID                   int32
	ProfileIDToMtProfile map[int32]*MtProfile
	OrgIDToMtProfile     map[int32]*[]*MtProfile
}

//---------------------------------------------------------------------------------------

// GetMtProfile get mtProfile for given mtProfileID
func (service *MockedService) GetMtProfile(mtProfileID int32) (message string, err error) {
	mtProfile, ok := service.ProfileIDToMtProfile[mtProfileID]
	if ok {
		jsonMtProfile, err := json.Marshal(mtProfile)
		if err != nil {
			return "", errors.New("MtProfile found, but could convert into JSON string")
		}
		return string(jsonMtProfile), nil
	}
	return "", fmt.Errorf("MtProfile not found for the ID: %v", mtProfileID)
}

//---------------------------------------------------------------------------------------

// GetMtProfiles get mtProfiles of given organization
func (service *MockedService) GetMtProfiles(organizationID int32) (message string, err error) {
	orgMtProfiles, ok := service.OrgIDToMtProfile[organizationID]
	if ok {
		jsonMtProfiles, err := json.Marshal(orgMtProfiles)
		if err != nil {
			return "", errors.New("MtProfile found, but could convert into JSON string")
		}
		return string(jsonMtProfiles), nil
	}
	return "", fmt.Errorf("MtProfile not found for the Organization ID: %v", organizationID)
}

//---------------------------------------------------------------------------------------

// CreateMtProfile create new mtProfile
func (service *MockedService) CreateMtProfile(organizationID int32, sourceLanguages, targetLanguages, sourceDefault, targetDefault string, glossaryID int32, tm, engines, apiKeys string) (message string, err error) {
	service.ID++
	mtProfile := &MtProfile{
		MtProfileID:     service.ID,
		OrganizationID:  organizationID,
		SourceLanguages: sourceLanguages,
		TargetLanguages: targetLanguages,
		SourceDefault:   sourceDefault,
		TargetDefault:   targetDefault,
		GlossaryID:      glossaryID,
		Tm:              tm,
		Engines:         engines,
		APIKeys:         apiKeys,
		CreatedDate:     time.Now(),
		LastModified:    time.Now(),
	}
	service.ProfileIDToMtProfile[service.ID] = mtProfile
	orgMtProfiles, ok := service.OrgIDToMtProfile[organizationID]
	if !ok {
		emptyOrgMtProfiles := make([]*MtProfile, 0, 1)
		orgMtProfiles = &emptyOrgMtProfiles
	}
	*orgMtProfiles = append(*orgMtProfiles, mtProfile)
	service.OrgIDToMtProfile[organizationID] = orgMtProfiles
	return fmt.Sprintf("MtProfile successfully created with ID: %v", service.ID), nil
}

//---------------------------------------------------------------------------------------

// DeleteMtProfile deletes an MtProfile
func (service *MockedService) DeleteMtProfile(mtProfileID int32) (message string, err error) {
	mtProfile, ok := service.ProfileIDToMtProfile[mtProfileID]
	if ok {
		delete(service.ProfileIDToMtProfile, mtProfileID)
		if len(*service.OrgIDToMtProfile[mtProfile.OrganizationID]) == 1 {
			delete(service.OrgIDToMtProfile, mtProfile.OrganizationID)
		} else {
			orgMtProfiles := service.OrgIDToMtProfile[mtProfile.OrganizationID]
			for idx, mtProfile := range *orgMtProfiles {
				if mtProfile.MtProfileID == mtProfileID {
					*orgMtProfiles = append((*orgMtProfiles)[:idx], (*orgMtProfiles)[idx+1:]...)
					break
				}
			}
		}
		return fmt.Sprintf("MtProfile with ID: %v successfully deleted ", mtProfileID), nil
	}
	return "", fmt.Errorf("MtProfile not found for the ID: %v", mtProfileID)
}

//---------------------------------------------------------------------------------------

// UpdateMtProfile updates an MtProfile
func (service *MockedService) UpdateMtProfile(mtProfileID int32, sourceLanguages, targetLanguages, sourceDefault, targetDefault string, glossaryID int32, tm, engines, apiKeys string) (message string, err error) {
	origMtProfile, ok := service.ProfileIDToMtProfile[mtProfileID]
	if ok {
		origMtProfile.SourceLanguages = sourceLanguages
		origMtProfile.TargetLanguages = targetLanguages
		origMtProfile.SourceDefault = sourceDefault
		origMtProfile.TargetDefault = targetDefault
		origMtProfile.GlossaryID = glossaryID
		origMtProfile.Tm = tm
		origMtProfile.Engines = engines
		origMtProfile.APIKeys = apiKeys
		origMtProfile.LastModified = time.Now()
		return fmt.Sprintf("MtProfile with ID: %v successfully updated", service.ID), nil
	}
	return "", fmt.Errorf("MtProfile not found for the ID: %v", mtProfileID)
}

//---------------------------------------------------------------------------------------
