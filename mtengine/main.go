// ai-portal/mtengine/main.go
package main

import (
	"log"
	"net/http"

	"ai-portal/mtengine/utils"

	"github.com/gorilla/mux"
)

//---------------------------------------------------------------------------------------

const (
	port = ":8000"
)

var mtProfileService utils.MtProfilService
var mtEngineService utils.MtEngineService

//---------------------------------------------------------------------------------------

// main function to run the service
func main() {
	mtProfileService = &utils.MockedService{
		ID:                   0,
		ProfileIDToMtProfile: make(map[int32]*utils.MtProfile, 0),
		OrgIDToMtProfile:     make(map[int32]*[]*utils.MtProfile, 0),
	}
	mtEngineService = &utils.MockedMtEngineService{
		MtProfileService: mtProfileService,
	}
	router := mux.NewRouter()
	router.HandleFunc("/mtengine", GetTranslation).Methods("POST")
	router.HandleFunc("/mtengine/{engineId}", GetEngineInfo).Methods("GET")
	router.HandleFunc("/mtengine", CreateEngine).Methods("PUT")
	router.HandleFunc("/mtengine", DeleteEngine).Methods("DELETE")
	router.HandleFunc("/mtprofile/{mtprofileId}", GetMtProfile).Methods("GET")
	router.HandleFunc("/mtprofile/organization/{organizationId}", GetMtProfiles).Methods("GET")
	router.HandleFunc("/mtprofile", CreateMtProfile).Methods("POST")
	router.HandleFunc("/mtprofile/{mtprofileId}", DeleteMtProfile).Methods("DELETE")
	router.HandleFunc("/mtprofile/{mtprofileId}", UpdateMtProfile).Methods("PUT")
	log.Fatal(http.ListenAndServe(port, router))
}

//---------------------------------------------------------------------------------------
